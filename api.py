#!/home/api/bin/python
#coding:utf-8

import os
from flask import Flask, request, redirect, url_for
from werkzeug import secure_filename
from flask.ext.mail import Mail
from flask.ext.mail import Message
from threading import Thread


UPLOAD_FOLDER = '/home/api/uploads'
ALLOWED_EXTENSIONS = set(['txt','zip','jpg','jpeg','png'])
app=Flask(__name__)
app.config['MAIL_SERVER'] = 'smtp服务器地址'
app.config['MAIL_PORT'] = smtp服务器端口 
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True
app.config['MAIL_USERNAME'] = 'smtp服务器用户名'
app.config['MAIL_PASSWORD'] = 'smtp服务器密码' 
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

mail = Mail(app)
to_mail=list()

def send_async_email(app, msg):
    with app.app_context():
        mail.send(msg)

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

@app.route('/')
def echohelp():
      return """此api接口有2种调用方式,一种是带附件的,一种是不带附件的,并且支持上传文件,访问uri为/uploads就可以上传你所需要的附件了,
然后在调用第一种方法发送带附件的邮件,具体如何使用如下:<br><br>1、/api/to=收件人1,收件2,收件3......&sub=发送的标题&text=发送的内容&att
ach=uploads上传过的附件名<br><br>2、/api/to=收件人1,收件2,收件3......&sub=发送的标题&text=发送的内容"""

@app.route('/uploads',methods=['GET', 'POST'])
def uploads():
       if request.method == 'POST':
       	file = request.files['file']
        if file and allowed_file(file.filename):
        	filename = secure_filename(file.filename)
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
                return "上传文件名为:"+filename 

    #redirect(url_for('uploads'))
       return """
    <!doctype html>
    <title>请上传你的文件</title>
    <h1>请上传你所需要的文件</h1>
    <form action="" method=post enctype=multipart/form-data>
      <p><input type=file name=file>
         <input type=submit value=上传>
    </form>
    <p>%s</p>
    """ % "<br>".join(['txt','zip','jpg','jpeg','png'])  

@app.route('/api/to=<toaddr>&sub=<sub>&text=<text>/')
def api(toaddr,sub,text):
       to_mail = toaddr.split(',')
       msg = Message(sub,sender='monitoring.alarm@qq.com',recipients=to_mail)
       msg.body = text
       thr = Thread(target=send_async_email, args=[app,msg])
       thr.start()
       return "ok"

@app.route('/api/to=<toaddr>&sub=<sub>&text=<text>&attach=<attach>/')
def api_sendmail(toaddr,sub,text,attach):
       to_mail = toaddr.split(',')
       msg = Message(sub,sender='monitoring.alarm@qq.com',recipients=to_mail)
       msg.body = text 
       with app.open_resource(UPLOAD_FOLDER+"/"+attach) as fp:
          msg.attach(UPLOAD_FOLDER+"/"+attach,"text/plain",fp.read())
       thr = Thread(target=send_async_email, args=[app,msg])
       thr.start()
       return "ok"

if __name__ == '__main__':
     app.run(host='0.0.0.0')