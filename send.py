#!/home/api/bin/python
# -*- coding:utf-8 -*-

import smtplib
import sys
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
def sendmail(to, sub, text):
    fromaddr = '发送邮件的地址'
    toaddr = to
    username = 'smtp服务器账号'
    password = 'smtp服务器密码'
    me = "Monitoring Alarm"+"<"+fromaddr+">"
    server = smtplib.SMTP('smtp服务器ip地址:smtp服务器端口')
    server.login(username, password)
    msg = MIMEText(text,_subtype='plain',_charset='utf8')
    msg['From'] = me
    msg['To'] = toaddr
    msg['Subject'] = sub
    server.ehlo()
    server.starttls()
    server.ehlo()
    server.sendmail(me, toaddr, msg.as_string())
    server.quit()

if __name__ == '__main__':
    if len(sys.argv) < 5:
        sendmail(sys.argv[1], sys.argv[2], sys.argv[3])